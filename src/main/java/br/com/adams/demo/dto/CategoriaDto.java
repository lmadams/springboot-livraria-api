package br.com.adams.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CategoriaDto {

  private Long id;

  @NotEmpty(message = "O 'Nome' não pode ser vazio")
  private String nome;
}
