package br.com.adams.demo.exceptions;

import br.com.adams.demo.dto.ErroMessageDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Locale;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.EMPTY;

@Slf4j
@ControllerAdvice
public class ApplicationExceptionHandler {

  @ExceptionHandler(CustonException.class)
  public ResponseEntity<ErroMessageDto> handleValidacaoException(
      final CustonException exception, final Locale locale) {
    final ErroMessageDto errorMessage =
        ErroMessageDto.builder()
            .codigo(exception.getCodigo())
            .mensagem(exception.getMessage())
            .build();

    log.error(errorMessageToString(errorMessage), exception);
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  private String errorMessageToString(final ErroMessageDto errorMessage) {
    return ofNullable(errorMessage)
        .map(em -> String.format("%s - %s", em.getCodigo(), em.getMensagem()))
        .orElse(EMPTY);
  }
}
