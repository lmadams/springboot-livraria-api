package br.com.adams.demo.service.impl;

import br.com.adams.demo.domain.Categoria;
import br.com.adams.demo.exceptions.CustonException;
import br.com.adams.demo.repository.CategoriaRepository;
import br.com.adams.demo.repository.LivroRepository;
import br.com.adams.demo.service.CategoriaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CategoriaServiceImpl implements CategoriaService {

  private final CategoriaRepository repository;
  private final LivroRepository livroRepository;

  @Override
  public List<Categoria> listar() {
    return repository.findAll();
  }

  @Override
  public Categoria buscarPorId(final Long id) {
    return repository
        .findById(id)
        .orElseThrow(
            () ->
                new CustonException(
                    CustonException.REGISTRO_NAO_ENCONTRADO,
                    String.format("Categoria não encontrada para o id: %d", id)));
  }

  @Override
  public Categoria criar(final Categoria categoria) {
    return repository.save(categoria);
  }

  @Override
  public Categoria editar(final Long id, final Categoria categoriaEditado) {
    final Categoria categoriaOriginal = buscarPorId(id);

    categoriaOriginal.setNome(categoriaEditado.getNome());

    return repository.save(categoriaOriginal);
  }

  @Override
  public void excluir(final Long idCategoria) {
    final Long livrosComACategoria = livroRepository.countAllByCategoria_Id(idCategoria);
    log.info("Existem {} livros com a categoria informada", livrosComACategoria);

    if (livrosComACategoria > 0) {
      throw new CustonException(
          CustonException.REGISTRO_POSSUI_RELACIONAMENTO,
          String.format(
              "A categoria %d possui %d livro(s) atrelados", idCategoria, livrosComACategoria));
    }

    final Categoria categoria = buscarPorId(idCategoria);
    repository.delete(categoria);
  }
}
