package br.com.adams.demo.converter;

import br.com.adams.demo.converter.basic.AbstractConverter;
import br.com.adams.demo.domain.Categoria;
import br.com.adams.demo.dto.CategoriaDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CategoriaConverter extends AbstractConverter<Categoria, CategoriaDto> {

  @Override
  public Categoria toDomain(final CategoriaDto categoriaDto) {
    return Optional.ofNullable(categoriaDto)
        .map(dto -> Categoria.builder().id(dto.getId()).nome(dto.getNome()).build())
        .orElse(null);
  }

  @Override
  public CategoriaDto toDto(final Categoria categoria) {
    return Optional.ofNullable(categoria)
        .map(entity -> CategoriaDto.builder().id(entity.getId()).nome(entity.getNome()).build())
        .orElse(null);
  }
}
