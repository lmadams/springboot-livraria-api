package br.com.adams.demo.converter.basic;

import java.util.List;

public interface Converter<E, J> {

  E toDomain(J dto);

  J toDto(E entity);

  List<J> toDtoList(List<E> listEntity);

  List<E> toDomainList(List<J> listJson);
}
