package br.com.adams.demo.converter.basic;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractConverter<E, J> implements Converter<E, J> {

  @Override
  public List<J> toDtoList(List<E> listEntity) {
    return listEntity.stream().map(this::toDto).collect(Collectors.toList());
  }

  @Override
  public List<E> toDomainList(List<J> listJson) {
    return listJson.stream().map(this::toDomain).collect(Collectors.toList());
  }
}
