INSERT INTO categoria(id, nome) VALUES (1, 'RPG');

INSERT INTO livro(id, nome, preco, paginas, codigo, categoria_id) VALUES(1, 'Magic The Gathering Theros Beyond Death Bundle', 189.89, 0, 1, 1);
INSERT INTO livro(id, nome, preco, paginas, codigo, categoria_id) VALUES(2, 'Pokemon Espada E Escudo 1 Blister Triplo', 21.90, 0, 2, 1);

INSERT INTO categoria(id, nome) VALUES(2, 'Informática');

INSERT INTO livro(id, nome, preco, paginas, codigo, categoria_id) VALUES(3, 'Use A Cabeca Desenvolvendo Para Android', 199.99, 928, 3, 2);
INSERT INTO livro(id, nome, preco, paginas, codigo, categoria_id) VALUES(4, 'Introducao A Programacao Com Python', 73.00, 328, 4, 2);
INSERT INTO livro(id, nome, preco, paginas, codigo, categoria_id) VALUES(5, 'Flutter Na Pratica', 115.00, 368, 5, 2);

INSERT INTO categoria(id, nome) VALUES(3, 'Direito');

INSERT INTO livro(id, nome, preco, paginas, codigo, categoria_id) VALUES(6, 'Vade Mecum 2020 - Saraiva', 207.00, 2568, 6, 3);
INSERT INTO livro(id, nome, preco, paginas, codigo, categoria_id) VALUES(7, 'Pacote Anticrime Comentado - Nucci', 78.00, 150, 7, 3);

INSERT INTO categoria(id, nome) VALUES(4, 'Gastronomia');

INSERT INTO livro(id, nome, preco, paginas, codigo, categoria_id)VALUES(8, 'Hamburgueres - 50 Das Melhores Receitas', 24.90, 128, 8, 4);
INSERT INTO livro(id, nome, preco, paginas, codigo, categoria_id)VALUES(9, 'Culinaria Vegana Para Atletas', 65.00, 228, 9, 4);
