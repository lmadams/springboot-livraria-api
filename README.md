## Springboot-livraria-api

Projeto de API para utilização nos testes do Curso de Frontend para Angular e React .

## Postman

Para ver os testes do Postman para esta API, importe o arquivo que 
esta na raiz deste projeto `Curso_Frontend.postman_collection.json` na instação do seu Postman.